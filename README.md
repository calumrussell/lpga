
Supports two operations:

1. Fetching results of a tournament - tasks.get\_results - Accepts a tournaments or pipe with a list of tournaments in json format. Outputs to stdout or csv.
2. Fetching tournaments in active season - tasks.get\_tournaments - Outputs to stdout or csv. Can be piped to get\_results.

Example Usage:

```
    python3 -m tasks.get_tournaments
    python3 -m tasks.get_results -t [tournamentname]
    python3 -m tasks.get_tournaments | python3 -m tasks.get_results -p
    python3 -m tasks.get_tournaments | python3 -m tasks.get_results -p -c data.csv
```
