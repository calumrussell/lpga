import requests
from bs4 import BeautifulSoup

class Tournaments:

    @staticmethod
    def get_all():
        tourns = []
        url = "http://www.lpga.com/tournaments"
        page = requests.get(url)
        soup = BeautifulSoup(page.text, "html.parser")
        schedules = soup.findAll("div", class_="tournament-schedule")

        for s in schedules:
            rows = s.findAll("tr")
            for r in rows:
                if rows.index(r) == 0 : continue
                if r.attrs and r.attrs.get("class") and r.attrs['class'][0] == 'network-info': continue
                cell = r.findAll("td")[3]
                links = cell.findAll("a")
                if len(links) == 2:
                    link = links[1].attrs['href']
                else:
                    link = links[0].attrs['href']
                if link[:5] == "/play":
                    link = links[0].attrs['href']
                tourns.append({"tournamentname": link.split("/")[2]})
        return tourns
