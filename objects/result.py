import csv

class Result:

    def to_csv(self, outfile):
       
        keys = self.data[0].keys()
        with open(outfile, 'w') as csv_file:
            data_writer = csv.DictWriter(
                csv_file, 
                keys
            )
            data_writer.writeheader()
            data_writer.writerows(self.data)
        return

    def __init__(self, data):
        self.data = data 

