import requests
from bs4 import BeautifulSoup

def _score_parser(score):
    rounds = ['R1','R2','R3','R4']
    score_split = score.strip().split("-")
    return [(r,s.strip()) for r, s in zip(rounds, score_split)]

class Tournament:

    def get_results(self):
        player_names = []
        player_scores = []
        url = "http://www.lpga.com/tournaments/" + self.name + "/results"
        try:
            page = requests.get(url)
        except Exception:
            print("Something wrong with " + self.name + "...skipping")
            return []
        
        soup = BeautifulSoup(page.text, "html.parser")
        leaderboards = soup.findAll("tbody", class_="full-leaderboard")
        for l in leaderboards:
            names = [r.contents[0].strip() for r in l.findAll('strong', class_="player-name")]
            scores = [_score_parser(r.contents[0]) for r in l.findAll('td', class_="scores")]
            player_names.extend(names)
            player_scores.extend(scores)
            
        zipped = zip(player_names, player_scores)
        res = []
        for row in zipped:
            p = {}
            p['name'] = row[0]
            p['R1'] = row[1][0][1] if len(row[1]) > 0 else -1
            p['R2'] = row[1][1][1] if len(row[1]) > 1 else -1
            p['R3'] = row[1][2][1] if len(row[1]) > 2 else -1
            p['R4'] = row[1][3][1] if len(row[1]) > 3 else -1
            res.append(p)
        return res 
         
    def __init__(self, name):
        self.name = name
