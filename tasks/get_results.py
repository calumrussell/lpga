import argparse
import time
import sys
import json

from objects.tournaments import Tournaments 
from objects.tournament import Tournament
from objects.result import Result

"""
   Task for retriving a given tournament
   Accepts a tournament name with -t flag
   Accepts a pipe in json format
   {"tournamentname": "x"}
  
   Default behaviour is to print to stdout
   With -c flag, will output to a csv file passed
   to flag. Will write or overwrite, not append.
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--tournament", type=str, help="pass a LPGA tournament name")
    parser.add_argument("-p", "--pipe", help="pass a pipe", action='store_true')
    parser.add_argument("-c", "--csv", help="output to csv file")
    args = parser.parse_args()

    res = []
    if args.tournament:
        t = Tournament(args.tournament)
        res.extend(t.get_results())
        if args.csv:
            Result(res).to_csv(args.csv)
        else:
            print(res)

    elif args.pipe:
        tournament_string = sys.stdin.read().replace("\'", "\"").split("\n")
        for tourn in tournament_string:
            if not tourn: continue
            tournament_json = json.loads(tourn)
            t = Tournament(tournament_json['tournamentname'])
            res.extend(t.get_results())
        if args.csv:
            Result(res).to_csv(args.csv)
        else:
            print(res)

    else:
        print('No tournament passed')
    

