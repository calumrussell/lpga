import argparse
import time

from objects.tournaments import Tournaments 
from objects.tournament import Tournament
from objects.result import Result

"""
  Task for fetching all the tournaments from LPGA
  Seems to be only those tournaments that are current
  for the active season
  With the -c flag, this will output to the file
  passed to flag. Will write or overwrite, not append.
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--csv", help="outputs to csv")
    args = parser.parse_args()

    tourns = Tournaments.get_all()
    if args.csv:
        Result(tourns).to_csv(args.csv)
        print('Written tournaments csv to ' + args.csv)
    else:
        for t in tourns:
            print(t)

